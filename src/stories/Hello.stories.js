import Hello from "../components/Hello";

export default {
	title: "Global/Hello",
	component: Hello,
};

const Template = (args) => <Hello {...args} />;

export const White = Template.bind({});
White.args = {
	color: "white",
};

export const Red = Template.bind({});
Red.args = {
	color: "red",
};
