import React from "react";
import Navbar from "../components/Navbar";

export default {
	title: "Global/Header",
	component: Navbar,
	argTypes: {
		variant: {
			options: ["white", "black"],
		},
	},
};

const Template = (args) => <Navbar {...args} />;

export const Dark = Template.bind({});
Dark.args = {
	background: "black",
};

export const White = Template.bind({});
White.args = {
	background: "white",
};
