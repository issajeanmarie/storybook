import React from "react";
import Button from "../components/Button";

export default {
	title: "Global/Button",
	component: Button,

	parameters: {
		backgrounds: {
			values: [
				{ name: "Red", value: "red" },
				{ name: "Green", value: "green" },
			],
		},
	},

	argTypes: {
		primary: {
			options: [true, false],
		},
	},

	args: {
		primary: false,
		rounded: false,
	},
};

// Template usage
const Template = (args) => <Button {...args} />;

export const Default = Template.bind({});
Default.args = {};

export const Primary = Template.bind({});
Primary.args = {
	primary: true,
};

export const Rounded = Template.bind({});
Rounded.args = {
	rounded: true,
};
