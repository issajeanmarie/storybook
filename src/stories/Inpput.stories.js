import Input from "../components/Input";

export default {
	title: "Shared/Input",
	component: Input,
};

const Template = (args) => <Input {...args} />;

export const Bordered = Template.bind({});
Bordered.args = {
	bordered: true,
	height: 43,
};
