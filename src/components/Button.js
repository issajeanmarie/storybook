import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const StyledButton = styled.button`
	border: none;
	outline: none;
	cursor: pointer;
	color: ${(props) => (props.primary ? "white" : props.color || "black")};
	background-color: ${(props) =>
		props.primary ? "blue" : props.background || "rgb(220,220,220)"};
	border-radius: ${(props) => (props.rounded ? "50px" : "3px")};
	padding: ${(props) =>
		props.small ? "4px 12px" : props.medium ? "6px 24px" : "12px 32px"};
`;

const Button = ({
	title,
	primary,
	background,
	color,
	rounded,
	small,
	medium,
	large,
	onClick,
}) => {
	return (
		<StyledButton
			onClick={onClick}
			primary={primary}
			background={background}
			rounded={rounded}
			small={small}
			medium={medium}
			large={large}
			color={color}
		>
			{title || "Button"}
		</StyledButton>
	);
};

Button.propTypes = {
	title: PropTypes.string,
	primary: PropTypes.bool,
	background: PropTypes.string,
	color: PropTypes.string,
	onClick: PropTypes.func,
};

export default Button;
