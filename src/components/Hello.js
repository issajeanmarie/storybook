import React from "react";
import PropTypes from "prop-types";

const Hello = ({ color }) => {
	return <div style={{ color: `${color}` }}>Hello</div>;
};

Hello.propTypes = {
	color: PropTypes.string,
};

export default Hello;
