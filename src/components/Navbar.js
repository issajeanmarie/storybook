import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const StyledHeader = styled.nav`
	background-color: ${(props) => props.background || "black"};
	border-bottom: ${(props) =>
		props.background === "white" ? "1px solid rgba(200, 200, 200, 0.8)" : ""};
	height: 62px;
	width: 100%;
	display: flex;
	justify-content: space-between;
	padding: 0 128px;
	color: ${(props) =>
		props.background === "white" && !props.color
			? "black"
			: props.background === "black" && !props.color
			? "white"
			: props.color};
	font-size: 16px;
`;

const LeftSide = styled.div`
	display: flex;
	gap: ${(props) => (props.space ? `${props.space}px` : "16px")};
	align-items: center;
	flex: 1;
`;

const ContactButton = styled.button`
	background-color: white;
	outline: none;
	padding: 12px 42px;
	border-radius: ${(props) => (props.siButtonRounded ? "50px" : "3px")};
	font-weight: bold;
	border: ${(props) =>
		props.isNavBlack ? "1px solid rgba(200, 200, 200, 0.8)" : "none"};
	cursor: pointer;
`;

const RightSide = styled.div`
	flex: 1;
	display: flex;
	align-items: center;
	justify-content: flex-end;
`;

const Header = ({
	background = "black",
	color,
	space = 16,
	isCentered,
	siButtonRounded,
	isNavBlack = background === "white",
}) => {
	return (
		<StyledHeader color={color} background={background} isCentered={isCentered}>
			<LeftSide space={space}>
				<p style={{ fontWeight: "bold" }}>Home</p>
				<p>About</p>
				<p>Contact</p>
				<p>Phone us</p>
			</LeftSide>

			<RightSide>
				<ContactButton
					siButtonRounded={siButtonRounded}
					isNavBlack={isNavBlack}
				>
					Contact us
				</ContactButton>
			</RightSide>
		</StyledHeader>
	);
};

Header.propTypes = {
	color: PropTypes.string,
	space: PropTypes.number,
	isCentered: PropTypes.bool,
	isNavBlack: PropTypes.bool,
	siButtonRounded: PropTypes.bool,
	background: PropTypes.oneOf(["black", "white"]),
};

export default Header;
