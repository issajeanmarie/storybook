export const Issa = ({ primary, secondary }) => {
	return (
		<p
			style={{
				color: primary ? "#18a360" : secondary ? "#1aa5c4" : "black",
				fontFamily: "Arial",
			}}
		>
			This is Issa's first story
		</p>
	);
};
