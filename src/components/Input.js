import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const StyledInput = styled.input`
	border: ${(props) => (props.bordered ? "1px solid black" : "none")};
	width: 400px;
	margin: 12px;
	height: ${(props) => `${props.height}px`};
	outline: none;
	background-color: "white";
`;

const Input = ({ bordered, height = 42 }) => {
	return <StyledInput bordered />;
};

Input.propTypes = {
	bordered: PropTypes.bool,
	height: PropTypes.number,
};

export default Input;
