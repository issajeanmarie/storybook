import React from "react";
import Header from "./components/Navbar";
import "./App.css";
import Input from "./components/Input";

const App = () => {
	return <Input bordered />;
};

export default App;
